#include "utils.h"
#include <stdio.h>
#include <stdlib.h>

/* Sign extends the given field to a 32-bit integer where field is
 * interpreted an n-bit integer. */ 
int sign_extend_number( unsigned int field, unsigned int n) {
    int signedBit = (field >> (n - 1) ) & 1;
    int setter = ((1 << n) - 1);
    int value = field & setter;
    if (signedBit) {
        value = value | (~setter);
    }
    return value;
}

unsigned int get_bit_interval(uint32_t instruction_bits, int start, int len) {
    return (instruction_bits >> start) & ((1 << len) - 1);
}

/* Unpacks the 32-bit machine code instruction given into the correct
 * type within the instruction struct */ 
Instruction parse_instruction(uint32_t instruction_bits) {
    unsigned int opcode = get_bit_interval(instruction_bits, 0, 7);
    Instruction instruction;
    switch (opcode) {
        case 0x33:
            instruction.rtype.opcode = opcode;
            instruction.rtype.rd = get_bit_interval(instruction_bits, 7, 5);
            instruction.rtype.funct3 = get_bit_interval(instruction_bits, 12, 3);
            instruction.rtype.rs1 = get_bit_interval(instruction_bits, 15, 5);
            instruction.rtype.rs2 = get_bit_interval(instruction_bits, 20, 5);
            instruction.rtype.funct7 = get_bit_interval(instruction_bits, 25, 7);
            break;
        
        case 0x03:
        case 0x13:
        case 0x73:
            instruction.itype.opcode = opcode;
            instruction.itype.rd = get_bit_interval(instruction_bits, 7, 5);
            instruction.itype.funct3 = get_bit_interval(instruction_bits, 12, 3);
            instruction.itype.rs1 = get_bit_interval(instruction_bits, 15, 5);
            instruction.itype.imm = get_bit_interval(instruction_bits, 20, 12);
            break;
        
        case 0x23:
            instruction.stype.opcode = opcode;
            instruction.stype.imm5 = get_bit_interval(instruction_bits, 7, 5);
            instruction.stype.funct3 = get_bit_interval(instruction_bits, 12, 3);
            instruction.stype.rs1 = get_bit_interval(instruction_bits, 15, 5);
            instruction.stype.rs2 = get_bit_interval(instruction_bits, 20, 5);
            instruction.stype.imm7 = get_bit_interval(instruction_bits, 25, 7);
            break;
        
        case 0x63:
            instruction.sbtype.opcode = opcode;
            instruction.sbtype.imm5 = get_bit_interval(instruction_bits, 7, 5);
            instruction.sbtype.funct3 = get_bit_interval(instruction_bits, 12, 3);
            instruction.sbtype.rs1 = get_bit_interval(instruction_bits, 15, 5);
            instruction.sbtype.rs2 = get_bit_interval(instruction_bits, 20, 5);
            instruction.sbtype.imm7 = get_bit_interval(instruction_bits, 25, 7);
            break;
            
        case 0x37:
            instruction.utype.opcode = opcode;
            instruction.utype.rd = get_bit_interval(instruction_bits, 7, 5);
            instruction.utype.imm = get_bit_interval(instruction_bits, 12, 20);
            break;
            
        case 0x6f:
            instruction.ujtype.opcode = opcode;
            instruction.ujtype.rd = get_bit_interval(instruction_bits, 7, 5);
            instruction.ujtype.imm = get_bit_interval(instruction_bits, 12, 20);
            break;
            
        default:
            break;
    }
    return instruction;
}

/* Return the number of bytes (from the current PC) to the branch label using the given
 * branch instruction */
int get_branch_offset(Instruction instruction) {
    int imm5 = instruction.sbtype.imm5;
    int imm7 = instruction.sbtype.imm7;
    int imm4to1 = (imm5 >> 1) << 1;
    int imm11 = (imm5 & 1) << 11;
    int imm10to5 = (imm7 & ((1 << 6) - 1)) << 5;
    int imm12 = ((imm7 >> 6) & 1) << 12;
    unsigned int offset = 0 | imm4to1 | imm10to5 | imm11 | imm12;
    return sign_extend_number(offset, 13);
}

/* Returns the number of bytes (from the current PC) to the jump label using the given
 * jump instruction */
int get_jump_offset(Instruction instruction) {
    int imm = instruction.ujtype.imm;
    int imm19to12 = (imm & ((1 << 8) - 1)) << 12;
    int imm11 = ((imm >> 8) & 1) << 11;
    int imm10to1 = ((imm >> 9) & ((1 << 10) - 1)) << 1;
    int imm20 = ((imm >> 19) & 1) << 20;
    unsigned int offset = 0 | imm10to1 | imm11 | imm19to12| imm20;
    return sign_extend_number(offset, 21);
}

int get_store_offset(Instruction instruction) {
    int imm5 = instruction.stype.imm5;
    int imm7 = instruction.stype.imm7;
    int imm4to0 = imm5;
    int imm11to5 = imm7 << 5;
    unsigned int offset = 0 | imm4to0 | imm11to5;
    return sign_extend_number(offset, 12);
}

void handle_invalid_instruction(Instruction instruction) {
    printf("Invalid Instruction: 0x%08x\n", instruction.bits); 
}

void handle_invalid_read(Address address) {
    printf("Bad Read. Address: 0x%08x\n", address);
    exit(-1);
}

void handle_invalid_write(Address address) {
    printf("Bad Write. Address: 0x%08x\n", address);
    exit(-1);
}


