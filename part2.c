#include <stdio.h> // for stderr
#include <stdlib.h> // for exit()
#include "types.h"
#include "utils.h"
#include "riscv.h"

Byte get_byte(Address, int);
int get_interval(int, int, int);
void execute_rtype(Instruction, Processor *);
void execute_itype_except_load(Instruction, Processor *);
void execute_branch(Instruction, Processor *);
void execute_jal(Instruction, Processor *);
void execute_load(Instruction, Processor *, Byte *);
void execute_store(Instruction, Processor *, Byte *);
void execute_ecall(Processor *, Byte *);
void execute_lui(Instruction, Processor *);

void execute_instruction(uint32_t instruction_bits, Processor *processor,Byte *memory) {    
    Instruction instruction = parse_instruction(instruction_bits);
    switch(instruction.opcode) {
        case 0x33:
            execute_rtype(instruction, processor);
            break;
        case 0x13:
            execute_itype_except_load(instruction, processor);
            break;
        case 0x73:
            execute_ecall(processor, memory);
            break;
        case 0x63:
            execute_branch(instruction, processor);
            break;
        case 0x6F:
            execute_jal(instruction, processor);
            break;
        case 0x23:
            execute_store(instruction, processor, memory);
            break;
        case 0x03:
            execute_load(instruction, processor, memory);
            break;
        case 0x37:
            execute_lui(instruction, processor);
            break;
        default: // undefined opcode
            handle_invalid_instruction(instruction);
            exit(-1);
            break;
    }
}

void execute_rtype(Instruction instruction, Processor *processor) {
    Word rs1 = instruction.rtype.rs1;
    Word rs2 = instruction.rtype.rs2;
    Word rd = instruction.rtype.rd;
    int shiftVal = processor->R[rs2];
    int shiftMask = (1 << (32 - shiftVal)) - 1;
    long long int x;
    switch (instruction.rtype.funct3){
        case 0x0:
            switch (instruction.rtype.funct7) {
                case 0x0:
                    // Add
                    processor->R[rd] = processor->R[rs1] + processor->R[rs2];
                    break;
                case 0x1:
                    // Mul
                    processor->R[rd] = processor->R[rs1] * processor->R[rs2];
                    break;
                case 0x20:
                    // Sub
                    processor->R[rd] = processor->R[rs1] - processor->R[rs2];
                    break;
                default:
                    handle_invalid_instruction(instruction);
                    exit(-1);
                    break;
            }
            break;
        case 0x1:
            switch (instruction.rtype.funct7) {
                case 0x0:
                    // SLL
                    processor->R[rd] = processor->R[rs1] << processor->R[rs2];
                    break;
                case 0x1:
                    // MULH
                    x = processor->R[rs1] * processor->R[rs2];
                    x = x >> 32;
                    processor->R[rd] = x;
                    break;
            }
            break;
        case 0x2:
            // SLT
            processor->R[rd] = ((int) processor->R[rs1] < (int) processor->R[rs2]) ? 1 : 0;
            break;
        case 0x4:
            switch (instruction.rtype.funct7) {
                case 0x0:
                    // XOR
                    processor->R[rd] = processor->R[rs1] ^ processor->R[rs2];
                    break;
                case 0x1:
                    // DIV
                    processor->R[rd] = processor->R[rs1] / processor->R[rs2];
                    break;
                default:
                    handle_invalid_instruction(instruction);
                    exit(-1);
                    break;
            }
            break;
        case 0x5:
            switch (instruction.rtype.funct7) {
                case 0x0:
                    // SRL
                    if (shiftVal == 0) {
                        processor->R[rd] = processor->R[rs1];
                        break;
                    }
                    processor->R[rd] = (processor->R[rs1] >> shiftVal) & shiftMask;
                    break;
                case 0x20:
                    // SRA
                    if (shiftVal == 0) {
                        processor->R[rd] = processor->R[rs1];
                        break;
                    }
                    processor->R[rd] = sign_extend_number(processor->R[rs1] >> processor->R[rs2], 32 - processor->R[rs2]);
                    break;
                default:
                    handle_invalid_instruction(instruction);
                    exit(-1);
                break;
            }
            break;
        case 0x6:
            switch (instruction.rtype.funct7) {
                case 0x0:
                    // OR
                    processor->R[rd] = processor->R[rs1] | processor->R[rs2];
                    break;
                case 0x1:
                    // REM
                    processor->R[rd] = processor->R[rs1] % processor->R[rs2];
                    break;
                default:
                    handle_invalid_instruction(instruction);
                    exit(-1);
                    break;
            }
            break;
        case 0x7:
            // AND
            processor->R[rd] = processor->R[rs1] & processor->R[rs2];
            break;
        default:
            handle_invalid_instruction(instruction);
            exit(-1);
            break;
    }
    processor->PC = processor->PC + (4);
}

void execute_itype_except_load(Instruction instruction, Processor *processor) {
    int imm = sign_extend_number(instruction.itype.imm, 12);
    Word rs1 = instruction.itype.rs1;
    Word rd = instruction.itype.rd;
    int shift = get_interval(imm, 0, 5);
    int setter = (1 << (32 - shift)) - 1;
    switch (instruction.itype.funct3) {
        case 0x0:
            // ADDI
            processor->R[rd] = processor->R[rs1] + imm;
            break;
        case 0x1:
            if (get_interval(imm, 5, 7) != 0x00) {
                handle_invalid_instruction(instruction);
                exit(-1);
                break;
            }
            // SLLI
            processor->R[rd] = processor->R[rs1] << get_interval(imm, 0, 5);
            break;
        case 0x2:
            // SLTI
            processor->R[rd] = ((int) processor->R[rs1] < imm) ? 1 : 0;
            
            break;
        case 0x4:
            // XORI
            processor->R[rd] = processor->R[rs1] ^ imm;
            break;
        case 0x5:
            // Shift Right (You must handle both logical and arithmetic)
            switch (get_interval(imm, 5, 7)) {
                case 0x00:
                    // SRLI
                    if (shift == 0) {
                        processor->R[rd] = processor->R[rs1];
                        break;
                    }
                    processor->R[rd] = (processor->R[rs1] >> shift) & setter;
                    break;
                case 0x20:
                    // SRAI
                    if (shift == 0) {
                        processor->R[rd] = processor->R[rs1];
                        break;
                    }
                    processor->R[rd] = sign_extend_number((processor->R[rs1] >> shift), 32 - shift);
                    break;
                default:
                    handle_invalid_instruction(instruction);
                    exit(-1);
                    break;
            }
            break;
            
        case 0x6:
            // ORI
            processor->R[rd] = processor->R[rs1] | imm;
            break;
        case 0x7:
            // ANDI
            processor->R[rd] = processor->R[rs1] & imm;
            break;
        default:
            handle_invalid_instruction(instruction);
            break;
    }
    processor->PC = processor->PC + (4);
}

void execute_ecall(Processor *p, Byte *memory) {
    Register i;
    
    // syscall number is given by a0 (x10)
    // argument is given by a1
    switch(p->R[10]) {
        case 1: // print an integer
            printf("%d",p->R[11]);
            break;
        case 4: // print a string
            for(i=p->R[11];i<MEMORY_SPACE && load(memory,i,LENGTH_BYTE);i++) {
                printf("%c",load(memory,i,LENGTH_BYTE));
            }
            break;
        case 10: // exit
            printf("exiting the simulator\n");
            exit(0);
            break;
        case 11: // print a character
            printf("%c",p->R[11]);
            break;
        default: // undefined ecall
            printf("Illegal ecall number %d\n", p->R[10]);
            exit(-1);
            break;
    }
    p->PC = p->PC + (4);
}

void execute_branch(Instruction instruction, Processor *processor) {
    int offset = get_branch_offset(instruction);
    Word rs1 = instruction.sbtype.rs1;
    Word rs2 = instruction.sbtype.rs2;
    switch (instruction.sbtype.funct3) {
        case 0x0:
            // BEQ
            if (processor->R[rs1] == processor->R[rs2]) {
                processor->PC = processor->PC + (offset);
            } else {
                processor->PC = processor->PC + (4);
            }
            break;
        case 0x1:
            // BNE
            if (processor->R[rs1] != processor->R[rs2]) {
                processor->PC = processor->PC + (offset);
            } else {
                processor->PC = processor->PC + (4);
            }
            break;
        default:
            handle_invalid_instruction(instruction);
            exit(-1);
            break;
    }
}

void execute_load(Instruction instruction, Processor *processor, Byte *memory) {
    Word rd = instruction.itype.rd;
    Word rs1 = instruction.itype.rs1;
    int offset = sign_extend_number(instruction.itype.imm, 12);
    Address address = rs1 + offset;
    switch (instruction.itype.funct3) {
        case 0x0:
            // LB
            processor->R[rd] = sign_extend_number(load(memory, address, 1), 8);
            break;
        case 0x1:
            // LH
            processor->R[rd] = sign_extend_number(load(memory, address, 2), 16);
            break;
        case 0x2:
            // LW
            processor->R[rd] = load(memory, address, 4);
            break;
        default:
            handle_invalid_instruction(instruction);
            break;
    }
    processor->PC = processor->PC + (4);
}

void execute_store(Instruction instruction, Processor *processor, Byte *memory) {
    Word rs2 = instruction.stype.rs2;
    Word rs1 = instruction.stype.rs1;
    int offset = get_store_offset(instruction);
    Address address = rs1 + offset;
    switch (instruction.stype.funct3) {
        case 0x0:
            // SB
            store(memory, address, 1, get_interval(processor->R[rs2], 0, 8));
            break;
        case 0x1:
            // SH
            store(memory, address, 2, get_interval(processor->R[rs2], 0, 16));
            break;
        case 0x2:
            // SW
            store(memory, address, 4, processor->R[rs2]);
            break;
        default:
            handle_invalid_instruction(instruction);
            exit(-1);
            break;
    }
    processor->PC = processor->PC + (4);
}

void execute_jal(Instruction instruction, Processor *processor) {
    /* YOUR CODE HERE */
    Word rd = instruction.ujtype.rd;
    int offset = get_jump_offset(instruction);
    processor->R[rd] = processor->PC + 4;
    processor->PC = processor->PC + offset;

}

void execute_lui(Instruction instruction, Processor *processor) {
    /* YOUR CODE HERE */
    Word rd = instruction.utype.rd;
    int imm = instruction.utype.imm;
    imm = imm << 12;
    processor->R[rd] = 0 | imm;
    processor->PC = processor->PC + 4;
    
}

void store(Byte *memory, Address address, Alignment alignment, Word value) {
    /* YOUR CODE HERE */
    for (int i = 0; i < alignment; i++) {
        memory[address + i] = get_byte(value, i);
    }
}

Word load(Byte *memory, Address address, Alignment alignment) {
    /* YOUR CODE HERE */
    Word result = 0;
    int maskBase = 0xFF;
    int setter;
    for (int i = 0; i < alignment; i++) {
        setter = 0;
        setter = setter | memory[address + i];
        setter = setter << (8 * i);
        result = result & ~(maskBase << (8 * i));
        result = result | setter;
    }
    return result;
}

int get_interval(int integer, int start, int len) {
    return (integer >> start) & ((1 << len) - 1);
}

Byte get_byte(Address address, int location) {
    Byte selector = 0xFF;
    return (address >> (location * 8)) & selector;
}
